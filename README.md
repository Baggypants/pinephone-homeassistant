# pinephone-homeassistant

## Autologin

```bash
sudo dnf remove lightdm-mobile-greeter -y
sudo dnf install lightdm -y
```

edit `/etc/lightdm.conf`
```
[LightDM]

[Seat:*]
autologin-user=pine
autologin-session=phosh

[XDMCPServer]

[VNCServer]
```



Notes on turining a pinephone to be a homeassistant kiosk

<https://wiki.archlinux.org/title/GDM>


`firefox.desktop` goes in `~/.local/share/applications`

## battery

```
(venv) pine@pinephone:~/lnxlink$ cat /etc/rc.d/rc.local
#!/bin/bash
echo 4100000 > /sys/class/power_supply/axp20x-battery/voltage_max_design
```